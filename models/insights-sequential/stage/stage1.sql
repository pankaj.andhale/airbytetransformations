


SELECT
    Education AS Degree,
    AVG(Age) AS AvgAge,
    COUNT(*) AS RecordCount,
    AVG(CAST(EverBenched AS INT)) AS AvgBenchedPercentage,
    AVG(ExperienceInCurrentDomain) AS AvgExperience,
    COUNT(CASE WHEN LeaveOrNot = 1 THEN 1 ELSE NULL END) AS LeaveCount,
    COUNT(CASE WHEN LeaveOrNot = 0 THEN 1 ELSE NULL END) AS NoLeaveCount
FROM
  {{ ref( 'base' ) }}
GROUP BY
    Education