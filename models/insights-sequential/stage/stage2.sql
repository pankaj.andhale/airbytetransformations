


SELECT
    *,
     (CASE
    WHEN AvgExperience > 10 THEN 'High'
    WHEN AvgExperience > 5  THEN  'Medium'
    WHEN AvgExperience < 5 THEN    'Fresher'
    END ) as ExperienceCat
FROM
    {{ ref('stage1') }} 
    -- {{ ref('stage1') }}
